/*
* Copyright (c) 2018 Max Kunzelmann
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*
* Authored by: Max Kunzelmann <maxdev@posteo.de>
*/

public class MyApp : Gtk.Application {
    public MyApp() { 
        Object(
            application_id: "com.gitlab.maaxxs.gtk-hello",
            flags: ApplicationFlags.FLAGS_NONE
        );
    }

    protected override void activate() {
        // Create the Application Window
        var window = new Gtk.ApplicationWindow(this);
        window.default_height = 400;
        window.default_width = 600;
        window.title = _("Hello world");

        // Create a Button
        var button_hello = new Gtk.Button.with_label(_("Click me!"));
        button_hello.margin = 12;
        button_hello.clicked.connect(() => {
            button_hello.label = _("Hello World!");
            button_hello.sensitive = false;
        });

        // Create a Label
        var label = new Gtk.Label(_("Hello Again World."));

        // Add the Label and the Button
        // TODO: GtkApplication Window can only contain one widget at
        // a time. Create a Box to hold both widgets.
        window.add(label);
        window.add(button_hello);

        window.show_all();
    }

    public static int main(string[] args) {
        var app = new MyApp();
        return app.run(args);
    }
}
