
# First program in Vala

Using the [Getting Started Document](https://elementary.io/docs/code/getting-started)
from Elementary OS.

## Languages

If new translatable strings added, run in the build directory:

```
cd build
ninja com.gitlab.maaxxs.hello-gtk-pot
ninja com.gitlab.maaxxs.hello-gtk-update-po
```

## Build with Meson

**Elementary OS:**  
Install `elementary-sdk` and you're good to go to develop and build 
applications in Vala with GTK+.  

Configure the Build Directory with

```
meson build --prefix=/usr
```

Then we use `ninja` to build and install

```
cd build
ninja
sudo ninja install
```